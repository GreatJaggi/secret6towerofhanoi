﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BestScoreUI : MonoBehaviour
{
    public int tier;
    public Text pScoreText;
    public Text tScoreText;
    public GameObject highScoreWindow;
    int PScore;
    float TScore;

    void Start()
    {
        if(!PlayerPrefs.HasKey("P_BestScore" + tier)) {
            pScoreText.text = "N/A";
        }

        else    {
            PScore = PlayerPrefs.GetInt("P_BestScore" + tier);
            PScore += 1;
            pScoreText.text = PScore.ToString();
        }

        if(!PlayerPrefs.HasKey("T_BestScore" + tier)) {
            tScoreText.text = "N/A";
        }

        else    {
            TScore = PlayerPrefs.GetFloat("T_BestScore" + tier);
            string minutes = Mathf.Floor(TScore / 60).ToString("00");
            string seconds = (TScore % 60).ToString("00");
            string TScoreString = string.Format("{0}:{1}", minutes, seconds);
            tScoreText.text = TScoreString;
        }
        
    }

    public void CallHSWIndow()  {
        highScoreWindow.SetActive(true);
    }
}
