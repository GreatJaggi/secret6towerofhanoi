﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RingSliderMenu : MonoBehaviour
{
    public Slider ringSlider;
    public InputField ringIF;
    public Button ringSizeAcceptBtn;

    public Text warningText;
    int ringSize;

    void Start()    {
        ringSize = PlayerPrefs.HasKey("RingSize") ? PlayerPrefs.GetInt("RingSize") : 3;
        ringSlider.value = ringSize;
        SetRingSizeIF();
    }

    public void SizeValidation()    {
        ringSize = (int) ringSlider.value;
        if(PlayerPrefs.HasKey("RingSize"))  {
            if(ringSize != PlayerPrefs.GetInt("RingSize"))  {
                ringSizeAcceptBtn.interactable = true;
                warningText.enabled = true;

            }
            else    {
                ringSizeAcceptBtn.interactable = false;
                warningText.enabled = false;
            } 
        }
    }

    public void SetRingSize()  {
        if(PlayerPrefs.HasKey("RingSize"))
            PlayerPrefs.SetInt("RingSize", ringSize);
    }

    public void SetRingSizeIF() {
        ringIF.text = ringSlider.value.ToString();
    }
}
