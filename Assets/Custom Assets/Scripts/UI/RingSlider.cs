﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RingSlider : MonoBehaviour
{
    public RingUtility ringUtility;
    public Slider ringSlider;
    public InputField ringIF;
    public Button ringSizeAcceptBtn;

    public Text warningText;
    int ringSize;


    void Start()    {
        ringSize = ringUtility.ringSize;
        ringSlider.value = ringSize;
        SetRingSizeIF();

        ringSlider.interactable = PlayerPrefs.GetInt("GameMode") < 2 ? false : true;
        warningText.enabled = false;
    }

    public void SizeValidation()    {
        ringSize = (int) ringSlider.value;
        if(PlayerPrefs.HasKey("RingSize"))  {
            if(ringSize != PlayerPrefs.GetInt("RingSize"))  {
                ringSizeAcceptBtn.interactable = true;
                warningText.enabled = true;

            }
            else    {
                ringSizeAcceptBtn.interactable = false;
                warningText.enabled = false;
            } 
        }
    }

    public void SetRingSize()  {
        if(PlayerPrefs.HasKey("RingSize"))
            PlayerPrefs.SetInt("RingSize", ringSize);
            
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void SetRingSizeIF() {
        ringIF.text = ringSlider.value.ToString();
    }
}
