﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreMachine : MonoBehaviour
{
    int scorer;
    float gameTimer;
    public Text scoreText;
    public Text scoreTextGameOver;
    public Text endNote;
    // Start is called before the first frame update
    void Start()
    {
        scorer = 0;
        if(this.GetComponent<RingUtility>().ringSize == 3)
            PlayerPrefs.SetFloat("GameTime", 0.0f);

        gameTimer = PlayerPrefs.GetFloat("GameTime");

    }

    // Update is called once per frame
    void Update()
    {
        switch(PlayerPrefs.GetInt("GameMode"))  {
            case 0 : // perfectionist  
                scorer = PlayerPrefs.GetInt("MoveCount");

                scoreText.text = "Moves: <color=yellow>" + scorer + "</color>";
                scoreTextGameOver.text = scorer.ToString();
                
            break;

            case 1 : // timer
                if(this.GetComponent<PoleScript>().IsReadyToPlay()) 
                    gameTimer += Time.deltaTime;

                PlayerPrefs.SetFloat("GameTime", gameTimer);
 
                string minutes = Mathf.Floor(gameTimer / 60).ToString("00");
                string seconds = (gameTimer % 60).ToString("00");
     
                scoreText.text = "Time: <color=yellow>" + string.Format("{0}:{1}", minutes, seconds) + "</color>";
                scoreTextGameOver.text = string.Format("{0}:{1}", minutes, seconds);

            break;

            default : // practice
                scoreText.text = "Practice Mode";
            break;
        }
        
    }

    public void CreateEndNote(bool result) {
        endNote.text = result == true ? GamePreferences.GenerateGoodEndNote() : GamePreferences.GenerateBadEndNote();
    }

    public int GetCurrentMoveCount()    {
        return scorer;
    }

    public float GetCurrentGameTime()   {
        return gameTimer;
    }
}
