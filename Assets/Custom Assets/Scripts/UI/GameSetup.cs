﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSetup : MonoBehaviour
{
    public Dropdown goalDropDown;
    
    void Awake()    {
        goalDropDown.value = GamePreferences.WinGoal - 3;
    }

    public void SetGoal()   {
        GamePreferences.UpdateWinGoal(goalDropDown.value + 3);
    }
}
