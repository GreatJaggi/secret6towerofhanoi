﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private bool isPaused;

    [Header("UI Controls")]
    public GameObject mainCanvas;
    public GameObject pauseCanvas;
    public GameObject skipCanvas;

    private void Awake()    {
        Time.timeScale = 1;
    }

    private void Update()   {
        if(Input.GetKeyDown(KeyCode.Escape))
            PauseGame();
    }

    public void PauseGame() {
        isPaused = !isPaused;
        PauseLogic();
    }

     void PauseLogic()    {
        mainCanvas.SetActive(!isPaused);
        pauseCanvas.SetActive(isPaused);
        Time.timeScale = isPaused ? 0 : 1;
    }

    public bool IsPaused()  {
        return isPaused;
    }
    
    public void EndGame()   {
        mainCanvas.SetActive(false);
        pauseCanvas.SetActive(false);
        skipCanvas.SetActive(true);
    }

    public void RestartGame()   {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void QuitToMenu() {
        Time.timeScale = 1;
        SceneManager.LoadScene(0); // go back to menu
    }

    
}
