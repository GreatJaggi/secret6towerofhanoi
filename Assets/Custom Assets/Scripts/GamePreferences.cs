﻿using UnityEngine;
public static class GamePreferences {

    // Game Settings
    public static int WinGoal = PlayerPrefs.GetInt("WinGoal", 5);

    public static void UpdateWinGoal(int value)   {
        WinGoal = value;
        PlayerPrefs.SetInt("WinGoal", WinGoal);
    }

    public static string GenerateGoodEndNote()  {
        switch(Random.Range(0,5))   {
            case 0: return "High Score! Keep it up, maybe this is not the best Yet.";
            case 1: return "Hey, you got the high score! *Clap Clap*";
            case 2: return "You skills are amazing, You've got the high score!";
            case 3: return "Is this the best yet? You got the high score.";
            case 4: return "Wonderful! You got the best so far!";
        }

        return null;
    }

    public static string GenerateBadEndNote()   {
        switch(Random.Range(0,5))   {
            case 0: return "You're doing great. Keep it up!";
            case 1: return "Not bad. Maybe try one more time?";
            case 2: return "You almost got it! Try it again!";
            case 3: return "Please, Check your brain.";
            case 4: return "Aww, don't lose hope! Try again.";
        }

        return null;
    }
    

}
