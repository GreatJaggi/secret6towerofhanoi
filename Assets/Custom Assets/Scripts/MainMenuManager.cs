﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{

    public Text gameModeDesc;
    public Dropdown gameModeDropDown;
    void Awake()    {
        gameModeDropDown.value = PlayerPrefs.GetInt("GameMode");
        PlayerPrefs.SetInt("RingSize", 3);
    }
    public void StartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void SetGameMode()   {
        PlayerPrefs.SetInt("GameMode", gameModeDropDown.value);

        switch(gameModeDropDown.value)  {
            case 0:
                gameModeDesc.text = "Beat the game with the lowest possible moves!";
                break;
            case 1:
                gameModeDesc.text = "How fast can you solve from 3 up to 10 rings?";
                break;
            case 2: 
                gameModeDesc.text = "Try and practice with your target Ring Count.";
                break;
        }
    }
}
