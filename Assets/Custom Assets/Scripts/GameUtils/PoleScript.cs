﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PoleScript : MonoBehaviour
{
    [Header("Audio References")]
    public AudioSource winSFX;
    public AudioSource clackSFX;
    bool isDragging;

    public Stack<Ring>[] ringStacks = new Stack<Ring>[2];

    int currentIndex;
    
    [HideInInspector]
    public int moveCounter;

    Ring selectedRing;
    bool selecting;
    bool readyToPlay;

    void Start()    {
        ringStacks = new Stack<Ring>[this.GetComponent<RingUtility>().ringSize + 1];
        moveCounter = 0;

        if(this.GetComponent<RingUtility>().ringSize == 3)
                PlayerPrefs.SetInt("MoveCount", 0);

        moveCounter = PlayerPrefs.GetInt("MoveCount");

        for(int i = 0; i < ringStacks.Length; i++)  {
            ringStacks[i] = new Stack<Ring>();
        }

        readyToPlay = false;
        StartCoroutine(StartDelay());
    }

    IEnumerator StartDelay()    {
        yield return new WaitForSeconds(this.GetComponent<RingUtility>().ringSize * 0.5f);
        readyToPlay = true;
    }

    void Update()   {
        //print(readyToPlay);
        if(!this.GetComponent<GameManager>().IsPaused() && readyToPlay) {
            if(Input.GetMouseButtonDown(0) || ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began)))
                OnMouseDown();

            if(Input.GetMouseButtonUp(0) || ((Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Ended)))
                OnMouseUp();
        }
    }

    Vector3 GetActionPosition()    {
        if(Input.touchCount > 0)
            return Input.GetTouch(0).position;
        else return Input.mousePosition;
    }
    
    void OnMouseDown() {
        isDragging = true;

        Ray ray = Camera.main.ScreenPointToRay(GetActionPosition());
            RaycastHit hit;
        if (Physics.Raycast(ray, out hit))  
            switch(hit.collider.tag)    {
                case "Pole 1" : PullAndPopToStack(0); break;
                case "Pole 2" : PullAndPopToStack(1); break;
                case "Pole 3" : PullAndPopToStack(2); break;
            }
    }

    void OnMouseUp()    {
        isDragging = false;

        Ray ray = Camera.main.ScreenPointToRay(GetActionPosition());
            RaycastHit hit;
         if (Physics.Raycast(ray, out hit))  

            switch(hit.collider.gameObject.tag) {
                case "Pole 1" : 
                    if(selecting)
                        DropAndPushToStack(hit.collider, 0);
                break;

                case "Pole 2" : 
                    if(selecting)
                        DropAndPushToStack(hit.collider, 1);
                break;

                case "Pole 3" : 
                    if(selecting)
                        DropAndPushToStack(hit.collider, 2);
                break;

                default :
                    if(selecting)   {
                        ringStacks[currentIndex].Push(selectedRing);

                    selectedRing.ring.GetComponent<Rigidbody>().isKinematic = false;
                    selecting = false;
                    }
                        
                break;
            }
            
    }

    void DropAndPushToStack(Collider loc, int index)    {
        if(ringStacks[index].Count > 0) {
            if(selectedRing.stackNum < ringStacks[index].Peek().stackNum)   {
                ringStacks[index].Push(selectedRing);
                selectedRing.ring.transform.position = 
                new Vector3 (loc.gameObject.transform.position.x, 5f, loc.gameObject.transform.position.z);
                
                CheckStack(index);
                if(index != currentIndex)
                    moveCounter++;

                clackSFX.Play();
            }
            else{
                ringStacks[currentIndex].Push(selectedRing);
            }
        }
        
        else    {
            ringStacks[index].Push(selectedRing);
            selectedRing.ring.transform.position = 
                new Vector3 (loc.gameObject.transform.position.x, 5f, loc.gameObject.transform.position.z);

            if(index != currentIndex)
                    moveCounter++;
            clackSFX.Play();
        }
        
        selectedRing.ring.GetComponent<Rigidbody>().isKinematic = false;
        selecting = false;

        PlayerPrefs.SetInt("MoveCount", moveCounter);
    }

    void PullAndPopToStack(int index)    {
        if(ringStacks[index].Count > 0)  {
            selectedRing = ringStacks[index].Pop();
            selectedRing.ring.transform.position = new Vector3 (selectedRing.ring.transform.position.x, 7f, selectedRing.ring.transform.position.z);
            selectedRing.ring.GetComponent<Rigidbody>().isKinematic = true;
            selecting = true;
            currentIndex = index;
        }
    }

    void CheckStack(int index)   {
        
        if(index == 0) return; // it was the starting pole/peg.

        Stack<Ring> tempStackRing = new Stack<Ring>();
        Ring currentRing;

        List<Ring> ringList = new List<Ring>();

        while(ringStacks[index].Count > 0)  {
            currentRing = ringStacks[index].Pop();
            ringList.Add(currentRing);
            tempStackRing.Push(currentRing);
        }

        while(tempStackRing.Count > 0)  {
            ringStacks[index].Push(tempStackRing.Pop());
        }

        if(ringList.Count == this.GetComponent<RingUtility>().ringSize)
            for(int i = 0; i < ringList.Count; i++)
                if(i < ringList.Count - 1)
                    if(ringList[i].stackNum < ringList[i + 1].stackNum) {
                        if(i == ringList.Count - 2) {
                            winSFX.Play();
                            readyToPlay = false;

                            if(PlayerPrefs.GetInt("GameMode") < 2) {
                                if(this.GetComponent<RingUtility>().ringSize < GamePreferences.WinGoal)  {
                                    if(PlayerPrefs.HasKey("RingSize"))
                                        PlayerPrefs.SetInt("RingSize", this.GetComponent<RingUtility>().ringSize + 1);
                                        StartCoroutine(WaitAndLoadNextLevel(3));
                                }

                                else    {
                                    UpdateHighScore();
                                    this.GetComponent<GameManager>().EndGame();
                                }    
                            }
                            
                                
                        }
                            
                        continue;
                    }
    }

    void UpdateHighScore()   {
        
        switch(PlayerPrefs.GetInt("GameMode"))  {
            case 0 : // perfectionist
                if(PlayerPrefs.GetInt("P_BestScore" + GamePreferences.WinGoal, 999) > this.GetComponent<ScoreMachine>().GetCurrentMoveCount())  {
                    PlayerPrefs.SetInt("P_BestScore" + GamePreferences.WinGoal, this.GetComponent<ScoreMachine>().GetCurrentMoveCount());
                    this.GetComponent<ScoreMachine>().CreateEndNote(true);
                }
                else this.GetComponent<ScoreMachine>().CreateEndNote(false);
                    
            break;

            case 1 :
                if(PlayerPrefs.GetFloat("T_BestScore" + GamePreferences.WinGoal, 9909) > this.GetComponent<ScoreMachine>().GetCurrentGameTime())    {
                    PlayerPrefs.SetFloat("T_BestScore" + GamePreferences.WinGoal, this.GetComponent<ScoreMachine>().GetCurrentGameTime());
                    this.GetComponent<ScoreMachine>().CreateEndNote(true);
                }
                else this.GetComponent<ScoreMachine>().CreateEndNote(false);
                    
            break;
        }
    }

    public bool IsReadyToPlay() {
        return readyToPlay;
    }

    IEnumerator WaitAndLoadNextLevel(float duration)  {
        yield return new WaitForSeconds(duration);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
