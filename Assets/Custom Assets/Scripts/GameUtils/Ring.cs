﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ring
{
    public GameObject ring;
    public int stackNum;

    public Ring(GameObject _ring, int _stackNum)   {
        ring = _ring;
        stackNum = _stackNum;
    }
}
