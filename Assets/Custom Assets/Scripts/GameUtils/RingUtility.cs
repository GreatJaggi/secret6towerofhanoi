﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingUtility : MonoBehaviour
{
    [Range(3, 9)]
    public int ringSize;
    public GameObject[] rings = new GameObject[9];
    GameObject[] poles = new GameObject[3];

    PoleScript poleScript;

    void Awake()    {
        if(PlayerPrefs.HasKey("RingSize"))
            ringSize = PlayerPrefs.GetInt("RingSize");
        else PlayerPrefs.SetInt("RingSize", ringSize);
    }
    
    void Start()    {
        poles[0] = GameObject.FindGameObjectWithTag("Pole 1");
        poles[1] = GameObject.FindGameObjectWithTag("Pole 2");
        poles[2] = GameObject.FindGameObjectWithTag("Pole 3");
        
        poleScript = this.GetComponent<PoleScript>();

        StartCoroutine(PutRings());
    }

    IEnumerator PutRings()   {
        for(int i = ringSize; i > 0; i--)   {

            yield return new WaitForSeconds(.25f);

            GameObject ring = Instantiate(rings[i-1], 
                                    new Vector3 (poles[0].transform.position.x, 5f, poles[0].transform.position.z), 
                                    Quaternion.Euler(Vector3.right * -90));

            Color randomColor = new Color(Random.value, Random.value, Random.value, 1.0f);
            ring.GetComponent<Renderer>().material.color = randomColor;
            
            poleScript.ringStacks[0].Push(new Ring(ring, i));
        }
        
        yield return null;
    }
    
}


