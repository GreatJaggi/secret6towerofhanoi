﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMSingleton : MonoBehaviour
{
    private static BGMSingleton instance = null;
    public static BGMSingleton Instance {
        get {return instance;}
    }

    void Awake()    {
        if(instance != null && instance != this)    {
            Destroy(this.gameObject);
            return;
        }
        else instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
}
