﻿Shader "Sin Custom/PixellatorShader" {
	Properties{
		_MainTex("Texture", 2D) = "white" {}
		_Grid("Grid", Vector) = (0.02, 0.02, 0, 0)
		_Intensity("Intensity", Range(0.0001, 0.006)) = 0.006
	}

	SubShader{
		Pass	{
			CGPROGRAM

			#pragma vertex vert_img
			#pragma fragment frag
			#include "UnityCG.cginc"

			uniform sampler2D _MainTex;
			uniform float4 _Grid;

			float _Intensity;

			float4 frag(v2f_img i) : COLOR	{
				float u = ((i.uv.x / _Intensity) - fmod(i.uv.x, _Intensity) / (_Intensity)) * _Intensity;
				float v = ((i.uv.y / _Intensity) - fmod(i.uv.y, _Intensity) / (_Intensity)) * _Intensity;
				float4 col = tex2D(_MainTex, float2(u, v));
				return col;
			}
			ENDCG
		}
	}
}